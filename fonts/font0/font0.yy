{
    "id": "18a001b6-d02e-48d3-97ea-c21c14b10094",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Georgia",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "00f1dd6d-13e0-41bc-8b95-add5e6d40499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d0961661-1813-406e-a502-ef89306f68c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 224,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fbf4e77d-ab62-452d-99fe-3fec4761d1af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 216,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6e656539-65f1-4e28-a2c8-37b4f558a3e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 205,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3fca0a8c-3df0-4231-ab27-22f2cd149b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 195,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fcc3206a-cb00-4790-81c5-35489ecaba62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 180,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d3a65d29-7af8-46fa-b59f-333193043cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 166,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4748d699-e29e-40f8-bc88-8abbef9ac366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "553c2d78-7a2d-4aa1-ad90-bc3c408fd382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 153,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "60d450f8-7ab3-4117-ba8b-e258aafb2d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 145,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3e6a0291-fc22-4c3e-bf65-58311757ac13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 229,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ab0915b2-7511-468a-bf0c-25a8c90205c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8e5839a5-0b68-42f7-9a1a-78e71e939cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aced31f0-3412-447c-a4ec-0f807410971e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 110,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8952a84e-09a8-4487-851e-6ce9b0073c6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fd20676b-dbf3-464d-bcea-844d114c7d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 96,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1ce966d1-9e59-471a-96cc-554e66b12adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 84,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6a2e7ba1-1a91-4985-8c3b-0055d6bf4412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 76,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cf9b2bd8-e537-4214-a4d6-da1c1491548f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 65,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dd71d85e-13e6-4579-a54b-079734d66055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6bf2c774-3ffa-4f85-841b-8d1c0fa3b1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 43,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "36167e82-749a-46a4-a54d-8bafa07dc6b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 124,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "184a3327-2a4e-4b65-ad95-8583e8692feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 238,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d2b4cbed-2f33-40a8-b0e8-647a8cde8b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "51e77257-1ba5-475b-a1db-faddf64e3c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0407632a-1d01-4437-acf6-e306f0f07be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 7,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0ff29987-6e56-40fe-99f5-6d546474e981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dae2c606-6474-4ad3-9902-48391dd566b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 248,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "81450924-0fa5-4baf-843c-07da7fd62145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "72f64c6b-3e57-46ca-87c0-ced6b5057990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 227,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fb2d12fb-d0f6-4f92-9d87-26b2e756a9c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 217,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f07d9f4e-a05b-4ea1-8b59-e1842ddc35bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 208,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "516c5b85-d1a3-4da7-922d-5244208ff049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 193,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c90a2a71-464b-4be8-932e-cbc64931c973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 178,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c594ada8-afb6-4d33-8624-9b57dd5fe97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "115c2a16-462a-4d49-93d7-f5267a1a33de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 153,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0b0b4935-1aeb-46ec-8c33-9bc0772c4bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "621848e2-9f36-4c85-a446-b57fdc619e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f948435f-36d0-4d73-a9b3-025b6a072007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4dae10bc-365f-4f06-aa0b-9b86c9c93224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ed3d6c6b-5d6a-42b4-86f3-0cd29f08ee27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "afb89b14-03b6-45f8-a64a-97cc450c867d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "dcb55053-985d-4709-9ab7-cc664237d962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 67,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5cbdfa9d-4175-467c-91e2-436dc8c8bf19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1c2b0fcb-b236-4225-b8ca-8eb8fe854767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 41,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bc8cda77-8081-431c-9ee8-b56c0ab9c228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cc57d72a-b4fa-4488-b9f7-fb992fd98c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 28,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f043b931-f07f-488c-93d7-50b505ab81bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 14,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "23e520d5-6a01-4a23-a6d3-75b770faf38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d0e9907d-bf3f-49c7-b8e1-9daa171c164c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd527a67-2dc7-4bae-b758-710465ebb338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8a625819-7ad0-4697-8948-b3f717cc2780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7a43a231-64dd-4e0e-a9fb-94f79b91b1c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "98b58510-79fd-41ad-a0f9-bb2daa9ae6a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b3642a1b-5b84-475c-b824-05fc7ee2d241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2d195ef2-d81b-4cdc-a81a-f1bcdeb65340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fbb7c261-7f03-425b-a089-ef5925f3a36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2c9fdb60-e4de-4b1d-ba54-cb1fd51069bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5e3efea1-9346-4a22-8589-5dc28e40f963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b4b4c6e9-0bb4-4be6-81a9-d350aec0d0c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c1d07741-331f-401a-bdca-9320b979bdcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "dcea918c-a9d4-4559-96a9-ee644aaac976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d37f8677-4126-4416-aa3f-68299d06faa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "badab6ed-e216-41aa-b7e1-6cbd694faeed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4333ffa3-56e6-40c1-b7ee-e90beecc619a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9490abab-5440-448a-8e76-2bf443b69165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "60cb5d26-dcf4-4bab-bfb4-6c5f5df3ea1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "aee24c9a-f2f6-42ea-8a6b-7de952e51fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "44cee594-3615-450c-808e-2ecbf81cf0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "80eaf3b0-72ab-4a56-87d3-bf5797e55313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4b6d17e0-7a77-4755-b184-8f2f306ea524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 7,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2b4cd854-3dfe-4a83-a78d-117ddc3d21ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2f0adfcc-a53a-4f99-80f0-b6106ea2819b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 125,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a2ae938d-db19-4c88-922f-b3c3adcfc61c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2d9e239d-0df7-4b33-815e-09eab8a6c029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 233,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "03613626-a7b0-466f-8873-7080541dc8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 222,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3006ab48-f74a-4233-b712-cf0fe67fca12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 215,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d1804f6d-e553-4b8d-9ea3-65955a6b8e8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 199,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1697113f-6d1b-4b59-b951-f95544e7bae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ebdd2a02-d8b7-4ae1-aa42-518ac57682e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 176,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e508128f-741a-482a-abf7-4d87d775b3ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "162749ff-2e04-424c-9634-885a2552a6e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 153,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8b1367b2-59a5-4dde-ab02-75e01bd0f993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "67c027e1-e6af-4f74-b99a-3b484156d02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 241,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "14f6caad-0f56-4764-a00d-19625d07b7d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 136,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1e07d8f2-2242-49d8-86d8-fb1ed103f8b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 114,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fcb87bb8-4d6c-4de0-94b0-567fac11f243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 102,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ac864217-d7a5-4e6b-be55-0b7e17fa411c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4af20330-b2a3-4421-9a96-c17cfc6081c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f5a845c6-60b3-4899-bd76-13582eee7821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b8a8e505-6a78-44b0-b38f-037eae17d1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "915c53ec-81ab-4393-9c62-3289fb0613ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "be6372a3-5a37-4c12-9ae6-863ffaa5111e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 42,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cb379827-14c5-4391-8875-4ce47fe508d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 33,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2cf65a77-bd83-4730-96d5-c8904bb087f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 18,
                "y": 82
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2f33ed53-4c3f-494c-a8dc-db6ddd075628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 29,
                "y": 82
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}