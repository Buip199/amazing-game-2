{
    "id": "73341614-1f7a-4109-8dec-835f8f7625b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "555f0fb0-335e-43c8-ba33-bc75d3af17ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "64f7267e-1936-4b3c-820b-38bd78eb1ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "555f0fb0-335e-43c8-ba33-bc75d3af17ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767f8031-02e4-41ce-ab19-653b5bc25e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "555f0fb0-335e-43c8-ba33-bc75d3af17ab",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "09895b6f-1f04-4a3f-be7c-871191502516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "0d016e64-2871-437d-8218-b053bf14d0ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09895b6f-1f04-4a3f-be7c-871191502516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d1a8cb-bd8b-4e23-8c68-dfebf4a2d85b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09895b6f-1f04-4a3f-be7c-871191502516",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "86c097d0-7266-408b-8b93-6768e6c8aaeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "33502f9e-0303-4eab-b79d-a9bfb5bcd272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86c097d0-7266-408b-8b93-6768e6c8aaeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab195f04-88d4-4ad5-a9c1-b7cc034c79b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86c097d0-7266-408b-8b93-6768e6c8aaeb",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "9578c501-5661-4390-94ef-5a158b1463cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "cf54aaf5-f6c2-421e-b552-8565b38171e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9578c501-5661-4390-94ef-5a158b1463cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64fa163b-b8ed-409a-b18d-9adb1c07bbab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9578c501-5661-4390-94ef-5a158b1463cd",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "5d7e58a2-216b-4ba0-b354-7ecd951c29cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "c5697cae-f30e-41db-b952-1fd00813bed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7e58a2-216b-4ba0-b354-7ecd951c29cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a18f6e-d105-4fc2-98aa-203b96dc788b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7e58a2-216b-4ba0-b354-7ecd951c29cd",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "746a8705-3636-4a15-8251-37c0cd910c40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "0895c917-3fc7-40f6-ad9b-f35afae19bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "746a8705-3636-4a15-8251-37c0cd910c40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf3cc44-9789-4c71-8774-f7b7d66c6570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "746a8705-3636-4a15-8251-37c0cd910c40",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "a76b73df-866c-4e5b-950a-1d1a8fd332bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "ca775921-ea08-424a-b39a-05be8b9e2b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a76b73df-866c-4e5b-950a-1d1a8fd332bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1d07c4-e349-4599-ba24-5da16cc29923",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a76b73df-866c-4e5b-950a-1d1a8fd332bb",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        },
        {
            "id": "401db5b0-be60-4031-93f4-fd841659a549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "compositeImage": {
                "id": "9c9d1ec9-4f65-420d-8bc5-4d4e2098bd41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401db5b0-be60-4031-93f4-fd841659a549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c707b41-a5cd-497a-adfa-683253bfd3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401db5b0-be60-4031-93f4-fd841659a549",
                    "LayerId": "1db409ed-aee8-42ab-bbd0-c508628ac831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1db409ed-aee8-42ab-bbd0-c508628ac831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73341614-1f7a-4109-8dec-835f8f7625b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}