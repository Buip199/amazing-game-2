{
    "id": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c3e7c97-2625-44fe-b24f-f7c5ca3381a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "a4bde146-473c-4b22-8e17-f39d3a71d6d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c3e7c97-2625-44fe-b24f-f7c5ca3381a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33889939-651b-4db1-a7ef-1b3e9cf15c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c3e7c97-2625-44fe-b24f-f7c5ca3381a5",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "218055f3-6bc8-495a-a0d9-95c09b317d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "557c3e3e-d21c-409a-8643-79594e98e598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "218055f3-6bc8-495a-a0d9-95c09b317d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33fcfb99-df8a-4844-b416-ec8d73e1cb3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "218055f3-6bc8-495a-a0d9-95c09b317d00",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "8a929a31-13a8-4ce3-aff3-6f75f8eb84d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "c41746e1-9096-4534-875e-1d5ba9cffcea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a929a31-13a8-4ce3-aff3-6f75f8eb84d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e285743-d61b-40a1-9cce-9a929e265ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a929a31-13a8-4ce3-aff3-6f75f8eb84d2",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "ebbe0b49-ec3e-442b-92c1-da015f60237c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "bceded0c-bad9-442c-ab20-8ecab0af23da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbe0b49-ec3e-442b-92c1-da015f60237c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7e009e-c215-45c2-93a1-6fb3d52ce085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbe0b49-ec3e-442b-92c1-da015f60237c",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "9e2268e3-7a0e-4bc5-a588-d05eb5b92ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "987525eb-e563-423a-93a7-4c84a7ef2aeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2268e3-7a0e-4bc5-a588-d05eb5b92ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba65434-dc0d-48d8-ae56-7e2db72a742e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2268e3-7a0e-4bc5-a588-d05eb5b92ad3",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "c4598676-34e8-4bd4-ba3a-75750cffc4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "31a03ee1-aad1-46a2-bbe8-6a3d02ec4ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4598676-34e8-4bd4-ba3a-75750cffc4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de32855e-17e7-42ee-8d5d-e366f4ab1803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4598676-34e8-4bd4-ba3a-75750cffc4c2",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "7a034df9-6f12-4340-bd26-cf700928646e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "03f99284-b6bf-489f-ac2a-b7ad3cf5a4aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a034df9-6f12-4340-bd26-cf700928646e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b9c410-d204-4f9f-80ab-a1d9dd9a57ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a034df9-6f12-4340-bd26-cf700928646e",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        },
        {
            "id": "09a333ee-f70e-4ecd-94af-0963aa8fce2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "compositeImage": {
                "id": "fa388b18-dcb8-412d-8448-5dead799d4dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a333ee-f70e-4ecd-94af-0963aa8fce2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a100b0-855f-43c1-87b1-42e285f800d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a333ee-f70e-4ecd-94af-0963aa8fce2b",
                    "LayerId": "0ec508f3-ea25-4783-b69d-b3b31b1394e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0ec508f3-ea25-4783-b69d-b3b31b1394e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc01450f-79ae-4f98-9969-cf3e4a166ce1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}