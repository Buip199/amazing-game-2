{
    "id": "d412233f-798d-4d77-b211-5956845b9909",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7e62f52-fd0b-4dab-b283-6c2fb9353173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d412233f-798d-4d77-b211-5956845b9909",
            "compositeImage": {
                "id": "e6b69850-bb5e-4644-8969-eeff77ec958f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e62f52-fd0b-4dab-b283-6c2fb9353173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1fac9cc-8b0b-48aa-8c53-ee9a66df8a70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e62f52-fd0b-4dab-b283-6c2fb9353173",
                    "LayerId": "e1c94f21-e6d3-4818-a0b5-0be444094307"
                }
            ]
        },
        {
            "id": "cf015d43-3cfd-442e-922d-b9f2824af889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d412233f-798d-4d77-b211-5956845b9909",
            "compositeImage": {
                "id": "23afa89f-c197-4b2b-ad32-8b630ac5e33d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf015d43-3cfd-442e-922d-b9f2824af889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8070e6b-e5c3-416e-b553-6957ab883ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf015d43-3cfd-442e-922d-b9f2824af889",
                    "LayerId": "e1c94f21-e6d3-4818-a0b5-0be444094307"
                }
            ]
        },
        {
            "id": "1a3dbe60-e19a-4aa7-8c53-0e12d2b38de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d412233f-798d-4d77-b211-5956845b9909",
            "compositeImage": {
                "id": "40fa9c65-eabc-4d8e-984d-be6a9010b570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3dbe60-e19a-4aa7-8c53-0e12d2b38de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37eb826-6202-46f5-ba7a-cae0c8d29ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3dbe60-e19a-4aa7-8c53-0e12d2b38de8",
                    "LayerId": "e1c94f21-e6d3-4818-a0b5-0be444094307"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1c94f21-e6d3-4818-a0b5-0be444094307",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d412233f-798d-4d77-b211-5956845b9909",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}