{
    "id": "7a173862-0981-4147-9b91-0f6e3074e7c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_skeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e60cec3-194c-4563-ac73-8671b0e048a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "8f2d93de-3de9-4772-8ffd-3628c395a40a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e60cec3-194c-4563-ac73-8671b0e048a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e4f8ca5-aa9a-499f-abc8-91f48191792e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e60cec3-194c-4563-ac73-8671b0e048a1",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "cd4112e4-904c-4882-aa61-a0d1faeb7a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "98863382-b947-4b35-aef5-ae4bdbeeefa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4112e4-904c-4882-aa61-a0d1faeb7a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d6f214-85b9-41a4-8ff3-b81625c6693c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4112e4-904c-4882-aa61-a0d1faeb7a06",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "11bf0933-b269-42a4-9281-30bc4575f9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "817482ba-565e-4e53-980c-66dd09390610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11bf0933-b269-42a4-9281-30bc4575f9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc46139a-0035-4ced-9789-0d76e9e3b40d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11bf0933-b269-42a4-9281-30bc4575f9ea",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "a568eeb0-739d-40e4-aa12-328e9fa77cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "e1226a2a-9d99-4c5f-bc93-f2a2a2083b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a568eeb0-739d-40e4-aa12-328e9fa77cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef15af3d-f064-4c89-93e6-8fd62d097c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a568eeb0-739d-40e4-aa12-328e9fa77cba",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "8cea165e-c850-4c1a-9340-5bc8e6553720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "7f8d3d70-29e4-4d98-9df3-e00105c4ce97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cea165e-c850-4c1a-9340-5bc8e6553720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a32f2921-a87c-4790-880d-f8ffb9cee36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cea165e-c850-4c1a-9340-5bc8e6553720",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "4a881314-e785-4adf-a53d-d9696367921c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "4134080f-7640-4e3e-8fa6-aaece0023b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a881314-e785-4adf-a53d-d9696367921c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c14b43b-5a9a-4715-ad37-b709c28f3636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a881314-e785-4adf-a53d-d9696367921c",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "78353da3-e758-494b-94f4-19ea42bb2448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "dafca259-3049-49ca-854f-70cd2080b5e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78353da3-e758-494b-94f4-19ea42bb2448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9f0bdc-41e4-41a0-9a55-4487943f7bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78353da3-e758-494b-94f4-19ea42bb2448",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "e72c0e12-f378-433c-8a17-981ef4bbe0dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "2850f4b3-9326-4936-ad7a-29b34aa8ba27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72c0e12-f378-433c-8a17-981ef4bbe0dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3295ff-9ca2-4851-b393-bbb6cdb3d8cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72c0e12-f378-433c-8a17-981ef4bbe0dc",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "690b2348-5c0c-4f95-bee4-ed8982d9da23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "1a4dc2bb-ddd0-4182-a3e0-a6dbee275aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "690b2348-5c0c-4f95-bee4-ed8982d9da23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323ca517-b10d-4507-9812-a9f047da447b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "690b2348-5c0c-4f95-bee4-ed8982d9da23",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        },
        {
            "id": "f0e27ea1-634f-47b2-b65c-89ae0b4ac3d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "compositeImage": {
                "id": "59ce31ed-6ea7-495a-b945-5bde830f45eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e27ea1-634f-47b2-b65c-89ae0b4ac3d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f494d2ae-6a02-487e-bba3-930e5df0adaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e27ea1-634f-47b2-b65c-89ae0b4ac3d8",
                    "LayerId": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f0f67b82-a03c-4219-a6a1-d3adbb288ebb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}