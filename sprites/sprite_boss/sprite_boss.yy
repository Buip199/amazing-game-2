{
    "id": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 11,
    "bbox_right": 57,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6bd402d-15f7-4723-a326-c28e313e48be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "compositeImage": {
                "id": "396ab41e-afff-47f0-aa0d-abdc9c30f782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6bd402d-15f7-4723-a326-c28e313e48be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2525323-f777-4baa-8190-7f5e0e24ea0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6bd402d-15f7-4723-a326-c28e313e48be",
                    "LayerId": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2"
                }
            ]
        },
        {
            "id": "03fddd50-deeb-4cc2-a9ee-870322db02bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "compositeImage": {
                "id": "e0ab2aa4-8bcf-4fe5-bcf9-a19c5e1d4e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03fddd50-deeb-4cc2-a9ee-870322db02bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4867659-b139-4dcb-8103-2da4f449a44d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fddd50-deeb-4cc2-a9ee-870322db02bb",
                    "LayerId": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2"
                }
            ]
        },
        {
            "id": "e2341e81-d4b2-4ee7-807f-62b5110321c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "compositeImage": {
                "id": "b218fc5e-475a-46c1-ae47-54a61d0aba03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2341e81-d4b2-4ee7-807f-62b5110321c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4758585e-fcd7-453d-907b-3aadc6ddf3c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2341e81-d4b2-4ee7-807f-62b5110321c5",
                    "LayerId": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2"
                }
            ]
        },
        {
            "id": "0fe1b9dd-416e-4b1d-bcf5-a3e1bdb803af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "compositeImage": {
                "id": "b70c6493-00c9-4393-b173-27df95c704c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe1b9dd-416e-4b1d-bcf5-a3e1bdb803af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35b57386-2950-4298-8130-274f0d8f0cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe1b9dd-416e-4b1d-bcf5-a3e1bdb803af",
                    "LayerId": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2"
                }
            ]
        },
        {
            "id": "03e58324-1aa0-4b84-9941-cc22326808e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "compositeImage": {
                "id": "3af5a63c-0192-4999-8bb0-334846d9b71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e58324-1aa0-4b84-9941-cc22326808e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "108d03a6-79ed-4ce8-a1b3-41ff4ff4b501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e58324-1aa0-4b84-9941-cc22326808e1",
                    "LayerId": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2"
                }
            ]
        },
        {
            "id": "dc3ffaf9-0924-4154-ae20-bd67113804de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "compositeImage": {
                "id": "ae5fa618-a02a-4830-a9be-1268008734a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc3ffaf9-0924-4154-ae20-bd67113804de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5ea54b-f50c-46d5-8569-fee28b547668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc3ffaf9-0924-4154-ae20-bd67113804de",
                    "LayerId": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1062e63d-b9cc-4ca0-a97a-f9914f0571d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}