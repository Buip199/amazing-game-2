{
    "id": "90a17cab-3601-458f-8734-dbaf291ad844",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_paththrough",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5df71ff-656f-487e-ab03-e0ce7884f817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a17cab-3601-458f-8734-dbaf291ad844",
            "compositeImage": {
                "id": "203503c0-b4e3-47b6-aed6-0360fba25379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5df71ff-656f-487e-ab03-e0ce7884f817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b4cdfe-5671-4dde-99ef-1205f0690330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5df71ff-656f-487e-ab03-e0ce7884f817",
                    "LayerId": "ef0f92dc-28e0-4b45-9ac9-d498aabafe1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef0f92dc-28e0-4b45-9ac9-d498aabafe1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90a17cab-3601-458f-8734-dbaf291ad844",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}