{
    "id": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5eb1efc0-971f-4e48-8838-3f79be0eaddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "cdcfa981-ab55-4d56-a3ee-9a622fa0a615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb1efc0-971f-4e48-8838-3f79be0eaddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8ac43c-bb0d-437a-9fee-94bb91871760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb1efc0-971f-4e48-8838-3f79be0eaddf",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "176c8d91-67b0-418a-8a10-760ded4b301a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "2e7d8454-8ed0-438d-a79c-cc376adfa5e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176c8d91-67b0-418a-8a10-760ded4b301a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e34e070d-7f9b-4966-87ec-2d8fe1dc66fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176c8d91-67b0-418a-8a10-760ded4b301a",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "0d0890a9-794e-4b1b-97de-d947da19da64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "d1bdebff-11d5-4aa4-b31e-169950defb54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d0890a9-794e-4b1b-97de-d947da19da64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bee6eb-d7e5-4b02-b700-a7accb024a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d0890a9-794e-4b1b-97de-d947da19da64",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "abc44816-1c14-4b70-9fe4-24207a13ae61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "653bd029-c499-4b7c-811d-8643bfafe3d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc44816-1c14-4b70-9fe4-24207a13ae61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc862753-3c36-45fc-86dc-67b06a2e7b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc44816-1c14-4b70-9fe4-24207a13ae61",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "5421435b-e2b4-418a-a756-5ea169718e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "f409b877-177c-4852-a95f-a9773fd32236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5421435b-e2b4-418a-a756-5ea169718e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2eec0e1-ecdc-41a1-a2db-7e1ada7e718a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5421435b-e2b4-418a-a756-5ea169718e22",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "f3207042-b39a-4403-9994-039034371c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "6dc4b7bd-8a11-4fde-a65c-156eed4dc592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3207042-b39a-4403-9994-039034371c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "042c0359-88fd-49f4-a2df-75b91facca29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3207042-b39a-4403-9994-039034371c82",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "1decf712-159f-4231-b49b-e214a239e054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "b670a4ad-77f6-4cda-b46e-5d73b8eee4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1decf712-159f-4231-b49b-e214a239e054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9774db9a-7e74-4e70-85c9-1cec22b1a1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1decf712-159f-4231-b49b-e214a239e054",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        },
        {
            "id": "ea49bd86-bb16-4cdf-a802-592c3062cd56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "compositeImage": {
                "id": "b7546830-1830-438c-875c-2b2286334180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea49bd86-bb16-4cdf-a802-592c3062cd56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906066a0-b1e6-41ab-8355-f13ec7f6ceaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea49bd86-bb16-4cdf-a802-592c3062cd56",
                    "LayerId": "d7da60ef-f23c-43f2-8dbb-12e8f363b974"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d7da60ef-f23c-43f2-8dbb-12e8f363b974",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}