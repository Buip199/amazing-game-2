{
    "id": "e6aa302e-d661-4a6e-bda5-7ce6d4bf2682",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_red_switch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e881cf9-8e12-4642-a254-ef177dadc99e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6aa302e-d661-4a6e-bda5-7ce6d4bf2682",
            "compositeImage": {
                "id": "913be047-d489-46a1-8554-7777b88003d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e881cf9-8e12-4642-a254-ef177dadc99e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba78efd7-7bc7-433f-ae94-56db15fede95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e881cf9-8e12-4642-a254-ef177dadc99e",
                    "LayerId": "53686457-78f4-465d-9a1a-7bea68cf778f"
                }
            ]
        },
        {
            "id": "d61bca64-dd86-4222-8919-5bda70923d0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6aa302e-d661-4a6e-bda5-7ce6d4bf2682",
            "compositeImage": {
                "id": "6883fc87-e806-4e0f-a550-26b67b847cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61bca64-dd86-4222-8919-5bda70923d0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1fb8a9a-e726-4e6b-8569-c2937a2f3a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61bca64-dd86-4222-8919-5bda70923d0e",
                    "LayerId": "53686457-78f4-465d-9a1a-7bea68cf778f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "53686457-78f4-465d-9a1a-7bea68cf778f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6aa302e-d661-4a6e-bda5-7ce6d4bf2682",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}