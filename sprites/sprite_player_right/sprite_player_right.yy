{
    "id": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad4d171b-03c0-45a4-b7e0-776270ea518c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "571dbc82-3846-4fb8-8075-8ce76d9f6e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad4d171b-03c0-45a4-b7e0-776270ea518c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa858499-2f51-4c4e-ad41-7ec096232a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad4d171b-03c0-45a4-b7e0-776270ea518c",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "89e65a36-75fd-47fd-8390-c8701d91b55b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "22af186c-e455-46f2-b3fe-54395eb30021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e65a36-75fd-47fd-8390-c8701d91b55b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec343c73-d530-41f7-aa0b-310df3e715e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e65a36-75fd-47fd-8390-c8701d91b55b",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "54a9d479-27e9-4658-aeb6-d99f4423e5a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "dd0fa089-aff7-4709-87ca-57874c54a58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a9d479-27e9-4658-aeb6-d99f4423e5a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e65b92-de74-4bed-9ee7-ddbe8d63b303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a9d479-27e9-4658-aeb6-d99f4423e5a9",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "43866255-b106-4957-b54d-f865a830f05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "9b13ef3b-0699-4b3f-b998-ea63e9b58f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43866255-b106-4957-b54d-f865a830f05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eca72e1-85e6-49eb-ba18-d0ba5f4e1507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43866255-b106-4957-b54d-f865a830f05e",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "af4c5b33-d58e-4def-bccc-bb9096bf98b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "d8dd6685-ec7a-45f0-bae0-823141c68ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4c5b33-d58e-4def-bccc-bb9096bf98b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41119c02-93d3-4408-bc7a-053eb79aed13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4c5b33-d58e-4def-bccc-bb9096bf98b7",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "c844344f-85be-49df-a47e-5828f9df7b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "f5aa1522-b6c4-4c86-af37-3f533002cce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c844344f-85be-49df-a47e-5828f9df7b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c04a019-6a41-4458-9553-bdeba867a6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c844344f-85be-49df-a47e-5828f9df7b48",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "bf99a912-298e-4526-bc87-e8e3103c7e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "40130382-cd85-4c0a-ba5b-a7e2011dd9a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf99a912-298e-4526-bc87-e8e3103c7e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb883f2-cbf1-458f-af1e-d6f7122361c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf99a912-298e-4526-bc87-e8e3103c7e50",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        },
        {
            "id": "926371d6-157f-464b-b967-dfa6ce31cdd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "compositeImage": {
                "id": "4b2f9ad0-70de-42e8-acf8-0f31e88c04c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926371d6-157f-464b-b967-dfa6ce31cdd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e814bc97-bfb5-4e54-a861-3c43aaecbee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926371d6-157f-464b-b967-dfa6ce31cdd8",
                    "LayerId": "ba950c6e-14c5-4fe3-a900-1c67e2821494"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba950c6e-14c5-4fe3-a900-1c67e2821494",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5091d42-d9af-4a92-8fcf-2fe7fcb2ea00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}