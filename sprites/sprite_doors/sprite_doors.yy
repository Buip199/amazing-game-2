{
    "id": "322cba3c-4c34-405d-a94c-ba6ce9ba59d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_doors",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "009502d3-2ea3-41e9-951b-2e5bbd8073b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "322cba3c-4c34-405d-a94c-ba6ce9ba59d8",
            "compositeImage": {
                "id": "ef6cdd05-0921-4898-b8e4-c47e6a0562d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "009502d3-2ea3-41e9-951b-2e5bbd8073b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42ae39c-f1e1-4c0c-9928-ce8528219fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009502d3-2ea3-41e9-951b-2e5bbd8073b3",
                    "LayerId": "8a8b4edf-3a5f-4b07-bd01-f68958ab247c"
                }
            ]
        },
        {
            "id": "5b7395d8-48db-47c6-9638-98246b926828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "322cba3c-4c34-405d-a94c-ba6ce9ba59d8",
            "compositeImage": {
                "id": "446ff2c4-532e-4412-b30a-d1658ef61111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7395d8-48db-47c6-9638-98246b926828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b253a5a-4462-4d7c-abba-fa674dc3c1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7395d8-48db-47c6-9638-98246b926828",
                    "LayerId": "8a8b4edf-3a5f-4b07-bd01-f68958ab247c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a8b4edf-3a5f-4b07-bd01-f68958ab247c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "322cba3c-4c34-405d-a94c-ba6ce9ba59d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}