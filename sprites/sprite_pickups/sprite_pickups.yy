{
    "id": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_pickups",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa87fc11-ee61-46e2-9d51-dd7fe73d2361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "f8d8b72c-b634-4ec6-94e1-5ae4fe964015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa87fc11-ee61-46e2-9d51-dd7fe73d2361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff7fd54-43a1-46f9-9c90-20e69f2c9137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa87fc11-ee61-46e2-9d51-dd7fe73d2361",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "776b6b86-0885-4f90-a9b0-c51563e549bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "cf6283b9-c810-497c-b16e-816add9eb5b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "776b6b86-0885-4f90-a9b0-c51563e549bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d9ba495-78e6-4415-85c5-c2ef49d7ba64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776b6b86-0885-4f90-a9b0-c51563e549bd",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "189938bd-a615-41bd-b050-e922b7728ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "8b1d1bec-78c0-412b-977e-e09776937b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189938bd-a615-41bd-b050-e922b7728ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c330dab6-4498-4e27-831f-b63647c0f15f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189938bd-a615-41bd-b050-e922b7728ed0",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "a12788ca-5a37-4890-8cf9-7191c06f3ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "a7986424-f1d1-42fd-afba-e96f2ebf0174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a12788ca-5a37-4890-8cf9-7191c06f3ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "829181af-e101-41ea-8c63-6e21b6c9706a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a12788ca-5a37-4890-8cf9-7191c06f3ef1",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "ffe5df20-7c90-448a-93ad-f4540df20758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "94b13325-d58b-43e0-9221-dbfc2a3b4bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffe5df20-7c90-448a-93ad-f4540df20758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1162f157-f04f-4c30-8f4c-5c00667f049d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffe5df20-7c90-448a-93ad-f4540df20758",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "7f965e94-af5b-49eb-8468-9f6e9ed8fdc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "73727d57-9706-4a47-95a2-75e984c92189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f965e94-af5b-49eb-8468-9f6e9ed8fdc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307697cf-5777-46e9-9f4b-fca899aa8fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f965e94-af5b-49eb-8468-9f6e9ed8fdc5",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "79558aa4-327f-46ae-947b-3dbd4da03724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "e3191e63-816f-4c10-a6be-01a97866f3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79558aa4-327f-46ae-947b-3dbd4da03724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d946f46-2b23-42b7-b75f-e186ff1f4f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79558aa4-327f-46ae-947b-3dbd4da03724",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "f5402d55-2255-4557-9d1d-dbcb74b2b40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "c9c83c17-0d3d-4226-9598-c29715df6a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5402d55-2255-4557-9d1d-dbcb74b2b40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5802fb32-b64c-4e29-a15a-85aae9472af2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5402d55-2255-4557-9d1d-dbcb74b2b40f",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "b283802c-3568-4179-927f-8df14743e8f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "6f63c921-5dfd-4329-b7b1-cc8201b43830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b283802c-3568-4179-927f-8df14743e8f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41152b68-d739-4c95-8c01-adacb4ce4656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b283802c-3568-4179-927f-8df14743e8f5",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "6d6ecb81-08f4-442b-b8b6-3a9385940bf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "8dc387e9-bd5a-4803-ac1f-dddfe8944980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6ecb81-08f4-442b-b8b6-3a9385940bf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99187584-4209-478f-bb2c-9c2e71169a16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6ecb81-08f4-442b-b8b6-3a9385940bf3",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "24cb6d17-d76a-4f90-bba1-03b0146f3679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "c58079c7-9380-4350-8654-f64246087763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cb6d17-d76a-4f90-bba1-03b0146f3679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdb53b30-6682-4b73-8a6f-079f80da90b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cb6d17-d76a-4f90-bba1-03b0146f3679",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "951c6c48-c223-4f10-b68f-276b89a450a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "b506e771-10e2-47ab-a288-b3eb19f84d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951c6c48-c223-4f10-b68f-276b89a450a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce88fb4a-2613-4339-8143-cde0dc7922c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951c6c48-c223-4f10-b68f-276b89a450a4",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "6f03690c-6f5f-4f08-8b6d-afd02b0601ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "5ed89cd8-dd51-4406-b9a6-ba94c6233f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f03690c-6f5f-4f08-8b6d-afd02b0601ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45be5d88-4bd0-4936-a75f-b15dd8992544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f03690c-6f5f-4f08-8b6d-afd02b0601ca",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "16744e0c-8f0b-4360-aa54-bb198b098ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "748c12a5-0509-4454-967a-48e4ea49374e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16744e0c-8f0b-4360-aa54-bb198b098ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b34b22-e4bf-4c98-9a10-7739ee6523c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16744e0c-8f0b-4360-aa54-bb198b098ecd",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "dfbcafaf-51da-4187-8780-42abc3c7a10b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "83929bea-5ab1-4540-a4b1-7fa2c2c18596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbcafaf-51da-4187-8780-42abc3c7a10b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33031b6f-d0e0-47c0-827a-1460ecb1acd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbcafaf-51da-4187-8780-42abc3c7a10b",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "9877e37c-94ae-4bf8-abae-ee7cd60473d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "5dafc683-ce9f-4712-9eba-3342d0d3e052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9877e37c-94ae-4bf8-abae-ee7cd60473d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d4e3b5-c5ef-4d13-ba9c-1640d0d4856c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9877e37c-94ae-4bf8-abae-ee7cd60473d3",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "35a1bad5-b6b9-48f9-a043-a1e04f792cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "9bc09c31-1ffe-4178-8d61-525fdca21727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a1bad5-b6b9-48f9-a043-a1e04f792cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f265c805-aec9-422d-9010-26991f5201fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a1bad5-b6b9-48f9-a043-a1e04f792cf5",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "85315ea0-cd17-4546-97a5-2846992c135d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "8e1963d4-ffbb-4a9d-bc0d-3b3e92a20888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85315ea0-cd17-4546-97a5-2846992c135d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a286f79-01ee-4551-9bba-dfd6fd4a767a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85315ea0-cd17-4546-97a5-2846992c135d",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "32dcc207-17d9-4489-b52b-797cbceed09e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "2aa126d5-63c9-4af3-8365-bfdd4bff4ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32dcc207-17d9-4489-b52b-797cbceed09e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09918e3-ae68-4ed4-b069-d5bcf98dbd05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32dcc207-17d9-4489-b52b-797cbceed09e",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "ad3a4f8b-b6f8-408b-a523-fc71c49ff402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "23fa54b4-8130-4bdc-83b8-a0150dfa9714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad3a4f8b-b6f8-408b-a523-fc71c49ff402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5568594e-19d5-4fe2-9f84-0815a4a13c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad3a4f8b-b6f8-408b-a523-fc71c49ff402",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "2f52b270-674b-4085-bcc9-edf5ade7b772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "7ac315d3-531a-40ad-b398-e586243c3368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f52b270-674b-4085-bcc9-edf5ade7b772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ddec3b6-23cc-4453-86f9-943c70090e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f52b270-674b-4085-bcc9-edf5ade7b772",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "e2809cd6-cecb-460a-b42c-b29bdb8ee79c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "03bca21b-7f99-42b0-ac12-f5867bff0fdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2809cd6-cecb-460a-b42c-b29bdb8ee79c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0127868-d1a3-48ef-ad5b-6c14a758f49f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2809cd6-cecb-460a-b42c-b29bdb8ee79c",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "0cbc7d4a-00da-4dc6-8baa-c9ab495b5a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "dbd0c0af-a50e-45ed-9554-5aad9bddb4f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbc7d4a-00da-4dc6-8baa-c9ab495b5a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d2c422-37e9-484b-87b4-b1c024a12b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbc7d4a-00da-4dc6-8baa-c9ab495b5a6f",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "9743b857-0192-40b6-ad14-1218979aaa16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "33f435f9-44b7-4cb9-8675-414f27e399ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9743b857-0192-40b6-ad14-1218979aaa16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7756809-22da-4c45-95e7-996caee345ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9743b857-0192-40b6-ad14-1218979aaa16",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "25fa38bc-ff2c-4154-be6f-5fc69c697011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "7f52697a-c47c-4791-aebb-0cfef0ad9202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25fa38bc-ff2c-4154-be6f-5fc69c697011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcefeecc-bb53-4ece-b992-31d143d68651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25fa38bc-ff2c-4154-be6f-5fc69c697011",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "1032e55d-2ebd-4727-9c6b-5b30c2dfc79a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "a5d7f768-2a48-4277-943a-39fe204b5afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1032e55d-2ebd-4727-9c6b-5b30c2dfc79a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3d5d26-e27f-4493-bda4-f79b2d5f60f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1032e55d-2ebd-4727-9c6b-5b30c2dfc79a",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        },
        {
            "id": "6a869fc4-a0e1-4eec-b225-fef09de70732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "compositeImage": {
                "id": "81139630-2a1d-4a4f-8445-04a516d49408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a869fc4-a0e1-4eec-b225-fef09de70732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af0984dd-58cc-4ff1-ad93-0ec0cecca38a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a869fc4-a0e1-4eec-b225-fef09de70732",
                    "LayerId": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fcbbfb66-c09b-48d5-bbb8-d827ae020f49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}