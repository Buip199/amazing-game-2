{
    "id": "68b97c01-b2a8-42e5-a207-9944e80c9ef6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_door",
    "eventList": [
        {
            "id": "06f506da-19e3-4623-92f2-33d2e899775b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68b97c01-b2a8-42e5-a207-9944e80c9ef6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "322cba3c-4c34-405d-a94c-ba6ce9ba59d8",
    "visible": true
}