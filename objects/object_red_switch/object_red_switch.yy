{
    "id": "b9cbe264-5db5-455b-bd90-7dc0c612f86a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_switch",
    "eventList": [
        {
            "id": "f389342a-61e7-422b-a4e8-19a6af5f6f42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9cbe264-5db5-455b-bd90-7dc0c612f86a"
        },
        {
            "id": "eb6b7e0c-0521-4310-81be-5faed629db8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9cbe264-5db5-455b-bd90-7dc0c612f86a"
        },
        {
            "id": "28655d19-8ab1-4d1d-bce1-c925ce6d245d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b9cbe264-5db5-455b-bd90-7dc0c612f86a"
        },
        {
            "id": "5e5e8b4d-2740-4dbc-b77a-7b5bd664238e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b9cbe264-5db5-455b-bd90-7dc0c612f86a"
        },
        {
            "id": "cc0748b2-464d-46ae-94c7-698461015bcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b9cbe264-5db5-455b-bd90-7dc0c612f86a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6aa302e-d661-4a6e-bda5-7ce6d4bf2682",
    "visible": true
}