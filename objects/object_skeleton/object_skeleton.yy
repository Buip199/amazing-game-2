{
    "id": "db1ad220-d80b-45e7-bb95-ca06057e5d0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_skeleton",
    "eventList": [
        {
            "id": "86e84299-4daf-460b-960b-a6a6bb5dcba5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db1ad220-d80b-45e7-bb95-ca06057e5d0d"
        },
        {
            "id": "4a69974b-7f4c-4ae3-8bb4-86dbe24bb187",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "68b97c01-b2a8-42e5-a207-9944e80c9ef6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "db1ad220-d80b-45e7-bb95-ca06057e5d0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "576fb85c-db66-46c1-9a6a-de3d92571d07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a173862-0981-4147-9b91-0f6e3074e7c3",
    "visible": true
}