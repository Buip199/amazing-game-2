{
    "id": "57a7fc88-8e6e-485c-aff3-e3eedc4171ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall_1",
    "eventList": [
        {
            "id": "56748d8f-8df5-44ba-bc4f-852ce65cf9eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57a7fc88-8e6e-485c-aff3-e3eedc4171ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d412233f-798d-4d77-b211-5956845b9909",
    "visible": true
}