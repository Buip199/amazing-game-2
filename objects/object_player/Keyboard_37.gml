/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 7D1DD04C
/// @DnDArgument : "direction" "180"
direction = 180;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 3B16B2D0
/// @DnDArgument : "speed" "4"
speed = 4;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 0188BEF6
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 1D1E05D4
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_left"
/// @DnDSaveInfo : "spriteind" "dc01450f-79ae-4f98-9969-cf3e4a166ce1"
sprite_index = sprite_player_left;
image_index = image_index;