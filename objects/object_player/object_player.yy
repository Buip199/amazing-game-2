{
    "id": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "136999e0-1bfb-4394-affd-f04bb8ba7041",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "aaa63ebc-57ae-4efd-9b99-fbbbf3318333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "19fd2fd5-a162-4a4e-b758-57e0460a3002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "ff9710af-c77f-4437-ae19-ffc17e1892f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "74cdc674-90b0-44d1-8ceb-3e212212d112",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "dee184eb-16dd-4b3c-9f69-1476af2e3ee5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "7bc33cdc-f49b-42a6-a3ad-3fc02c020530",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "6365efa5-7a20-46ed-ad1a-7715ecbf8c2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "57a7fc88-8e6e-485c-aff3-e3eedc4171ed",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "ba3183d5-1db6-474a-b7dc-72e4fd9d45ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "623c053e-41b7-49c2-8cbd-e4de4effe367",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "7ce23237-32d4-47e3-9dca-32347b72d48e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "c5adaa46-b43f-4e3f-8f38-08e38653e92c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "58e6bd21-d0e3-4f10-9dd7-f1cb0712e803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "bef1be72-1d7f-4c60-9461-9bffa3210a6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "e0ffc35d-2d1a-4950-bea9-c5aa275109b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "576fb85c-db66-46c1-9a6a-de3d92571d07",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "4cfdbeea-ddad-415e-bee0-4263bd0d8e76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "68b97c01-b2a8-42e5-a207-9944e80c9ef6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        },
        {
            "id": "730e1abd-af14-44d2-8cc5-f3aab99d897f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "ef0f7079-2694-439d-a261-f908a0fa1045",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef8c05c2-859d-4b0d-a23f-57e65e8fa634"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72d49cb9-51b9-4b50-bdab-4c0313ed2222",
    "visible": true
}