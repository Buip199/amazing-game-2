{
    "id": "c5adaa46-b43f-4e3f-8f38-08e38653e92c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_door",
    "eventList": [
        {
            "id": "ae92c7f2-c4b9-4454-9ab6-27d8ae2bea33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5adaa46-b43f-4e3f-8f38-08e38653e92c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "322cba3c-4c34-405d-a94c-ba6ce9ba59d8",
    "visible": true
}