{
    "id": "007761ab-47be-411a-90e7-3e62861348af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_boss",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "576fb85c-db66-46c1-9a6a-de3d92571d07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4d18fdc-9a8a-4261-a841-f9ea5e35ea02",
    "visible": true
}