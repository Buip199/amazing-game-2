/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 790AC20B
/// @DnDArgument : "code" "image_speed = 0;$(13_10)$(13_10)random_image = irandom_range(1, image_number - 1);$(13_10)image_index = random_image;$(13_10)$(13_10)if (image_index > 0 && image_index < 9) {$(13_10)	type = "points";$(13_10)}$(13_10)else if (image_index > 8 && image_index < 15) {$(13_10)	type = "health";$(13_10)}$(13_10)else if (image_index > 14 && image_index < 17) {$(13_10)	type = "lives";$(13_10)}$(13_10)else if (image_index > 16 && image_index < 20) {$(13_10)	type = "damage";$(13_10)}$(13_10)else if (image_index > 19 && image_index < 22) {$(13_10)	type = "death";$(13_10)}$(13_10)else if (image_index > 21 && image_index < 25) {$(13_10)	type = "transport";$(13_10)}$(13_10)else if (image_index == 25) {$(13_10)	type = "speed"$(13_10)}$(13_10)else {$(13_10)	type = "mystery";$(13_10)}$(13_10)"
image_speed = 0;

random_image = irandom_range(1, image_number - 1);
image_index = random_image;

if (image_index > 0 && image_index < 9) {
	type = "points";
}
else if (image_index > 8 && image_index < 15) {
	type = "health";
}
else if (image_index > 14 && image_index < 17) {
	type = "lives";
}
else if (image_index > 16 && image_index < 20) {
	type = "damage";
}
else if (image_index > 19 && image_index < 22) {
	type = "death";
}
else if (image_index > 21 && image_index < 25) {
	type = "transport";
}
else if (image_index == 25) {
	type = "speed"
}
else {
	type = "mystery";
}