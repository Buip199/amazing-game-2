{
    "id": "ef0f7079-2694-439d-a261-f908a0fa1045",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_pickups",
    "eventList": [
        {
            "id": "bd0213b2-49bd-4f51-8ab6-94fa828bccc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef0f7079-2694-439d-a261-f908a0fa1045"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3af1c304-8072-4876-b4d1-e0934ceb8e7c",
    "visible": true
}