{
    "id": "623c053e-41b7-49c2-8cbd-e4de4effe367",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall_2",
    "eventList": [
        {
            "id": "2a9036ea-aa3b-4908-ac55-0ca4badd4df3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "623c053e-41b7-49c2-8cbd-e4de4effe367"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90a17cab-3601-458f-8734-dbaf291ad844",
    "visible": true
}