{
    "id": "952fc05c-68c8-4663-960d-b43cbeb14c1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_game_state",
    "eventList": [
        {
            "id": "2254a2d7-3111-4dc4-9f0c-fcf9a4dd89c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "952fc05c-68c8-4663-960d-b43cbeb14c1c"
        },
        {
            "id": "7b6b2b8f-0c57-42dc-b5b7-0def05b856ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "952fc05c-68c8-4663-960d-b43cbeb14c1c"
        },
        {
            "id": "fd44b441-f831-4724-b4ea-75bc2d336a2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "952fc05c-68c8-4663-960d-b43cbeb14c1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}