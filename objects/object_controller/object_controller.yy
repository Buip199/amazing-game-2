{
    "id": "1551dfdd-4e77-4a87-92ca-1a1e87dfc7af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_controller",
    "eventList": [
        {
            "id": "da4c72e3-af16-41bd-b5d9-b9566ac14173",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1551dfdd-4e77-4a87-92ca-1a1e87dfc7af"
        },
        {
            "id": "e7a804ed-9734-4623-acd5-de61a3c8b17b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1551dfdd-4e77-4a87-92ca-1a1e87dfc7af"
        },
        {
            "id": "3cd6fa60-86a9-48da-8396-6126db121259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1551dfdd-4e77-4a87-92ca-1a1e87dfc7af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}