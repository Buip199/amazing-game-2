/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1D4450D8
/// @DnDArgument : "code" "if (object_game_state.state == "game") {$(13_10)	$(13_10)	draw_set_font(font_main);$(13_10)	$(13_10)	draw_set_halign(fa_left);$(13_10)$(13_10)	draw_healthbar(x, y, x + 100, y + 32, p_health,$(13_10)	c_gray, c_red, c_lime, 0, true, true);$(13_10)$(13_10)	for (var i = 0; i < p_lives; i++) {$(13_10)		draw_sprite(sprite_player_down, 0, x + 5 + i * 32, y + 50);$(13_10)	}$(13_10)$(13_10)	draw_text( x, y + 80, "Score: " + string(p_score) );$(13_10)}"
if (object_game_state.state == "game") {
	
	draw_set_font(font_main);
	
	draw_set_halign(fa_left);

	draw_healthbar(x, y, x + 100, y + 32, p_health,
	c_gray, c_red, c_lime, 0, true, true);

	for (var i = 0; i < p_lives; i++) {
		draw_sprite(sprite_player_down, 0, x + 5 + i * 32, y + 50);
	}

	draw_text( x, y + 80, "Score: " + string(p_score) );
}