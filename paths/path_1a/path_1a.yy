{
    "id": "13cefccd-edd3-4095-8d8a-10a4576204ea",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_1a",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "5af19235-ab3e-4e32-ab5d-f09dfa4c84f5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 192,
            "speed": 100
        },
        {
            "id": "6137b92b-0098-4a23-a39f-4ffb43dbf197",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 192,
            "speed": 100
        },
        {
            "id": "fb984652-4df3-4371-870d-d4fb729baaa7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 192,
            "speed": 100
        },
        {
            "id": "e409dc6c-5aaf-47f0-ad1c-fa4d10a287eb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 192,
            "speed": 100
        },
        {
            "id": "9be5a8e0-869d-46b7-9351-5907ae9fcf4a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 160,
            "speed": 100
        },
        {
            "id": "8ca56d6e-1fc7-41fa-b241-20cc5d37fe81",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 160,
            "speed": 100
        },
        {
            "id": "1b092f52-3964-49b1-9188-8ab7bde0de39",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 160,
            "speed": 100
        },
        {
            "id": "9a8d9e07-35bc-4f3b-ad34-34af0f8115ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 192,
            "speed": 100
        },
        {
            "id": "121fed74-2ce9-47ed-a45c-4e34e89e5e5b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 192,
            "speed": 100
        },
        {
            "id": "24fa73ff-2ebb-4374-b0ad-72f568818bb7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 224,
            "speed": 100
        },
        {
            "id": "d0485a1b-e217-4872-a392-8f6703094200",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 224,
            "speed": 100
        },
        {
            "id": "5057e2f7-9f55-40bb-8beb-6d69f9093f2e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 256,
            "speed": 100
        },
        {
            "id": "1236d78d-4a69-4da9-8537-5f83e5915d65",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 256,
            "speed": 100
        },
        {
            "id": "c4793750-71e8-4f29-8248-ea67287b9fb2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 288,
            "speed": 100
        },
        {
            "id": "9601c5d9-a775-4c86-a57b-2b401b417607",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 320,
            "speed": 100
        },
        {
            "id": "4215a0d6-8077-4ca9-9b64-05dc56af8392",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 320,
            "speed": 100
        },
        {
            "id": "22440157-f246-46c1-8026-446406f1610d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 352,
            "speed": 100
        },
        {
            "id": "09a5e7fd-0f47-4e24-9799-1c141837d221",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 352,
            "speed": 100
        },
        {
            "id": "6020e2f7-ced9-49a8-8b75-2c6d57b1b561",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 384,
            "speed": 100
        },
        {
            "id": "64218240-6591-416d-abdb-3d3178572e16",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 416,
            "speed": 100
        },
        {
            "id": "4a67e17c-5fe3-4e2d-b02a-030dae77ecfb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 416,
            "speed": 100
        },
        {
            "id": "ef275437-5043-4abf-8d54-5ac375f4d6dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 448,
            "speed": 100
        },
        {
            "id": "4588bafa-dda2-4553-b91a-e040526a3c2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 448,
            "speed": 100
        },
        {
            "id": "29be3b99-37bb-43c1-90e0-b86c4d880548",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 480,
            "speed": 100
        },
        {
            "id": "7c048b3a-b28d-437b-944f-5eb60dcffb33",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 480,
            "speed": 100
        },
        {
            "id": "9979faa9-545e-4d7f-872a-89fb02dc0fc1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 480,
            "speed": 100
        },
        {
            "id": "d0cfbc0d-7c34-4a60-92af-b1ce4e69287d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 448,
            "speed": 100
        },
        {
            "id": "7776d77b-3f65-4769-8a01-78c1cb211886",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 448,
            "speed": 100
        },
        {
            "id": "cff21bb2-ba7f-4ef7-8e47-eb7ad5e9ac93",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 448,
            "speed": 100
        },
        {
            "id": "c3d0d31d-501f-4d13-8ee3-1ade3ca5d202",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 384,
            "speed": 100
        },
        {
            "id": "2e92221b-7cbe-4c22-9899-cb061ac126ac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 384,
            "speed": 100
        },
        {
            "id": "10c48743-7c59-4c2f-8d3d-f15b9fda86e1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 352,
            "speed": 100
        },
        {
            "id": "bf7f6569-ebee-48b5-aaac-f177bec27115",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 352,
            "speed": 100
        },
        {
            "id": "62efd1fb-15b5-411d-bbcb-d8d44b4f9e8d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 320,
            "speed": 100
        },
        {
            "id": "92433d38-bbf4-4d40-a0cd-9ef4715635f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 320,
            "speed": 100
        },
        {
            "id": "3d5d699b-5cda-4184-b62a-e7325e1c0e69",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 320,
            "speed": 100
        },
        {
            "id": "e9d6762f-61cf-4e6c-af9a-2e38118b56a7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 320,
            "speed": 100
        },
        {
            "id": "ca7a1169-4035-458a-b036-a1cd729f037c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 320,
            "speed": 100
        },
        {
            "id": "6286b3ab-3239-4bd7-839b-0fea7b5cb35f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 352,
            "speed": 100
        },
        {
            "id": "fea421c0-c91e-4cd3-88b4-a8186343ad10",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 352,
            "speed": 100
        },
        {
            "id": "24218c05-f045-4bec-80b8-2e0f07eace40",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 352,
            "speed": 100
        },
        {
            "id": "e00997d3-54e5-4677-8364-53b202c4392e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 384,
            "speed": 100
        },
        {
            "id": "c5ac444d-ed21-4773-81d2-38b428712212",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 384,
            "speed": 100
        },
        {
            "id": "8b23b8df-073c-467b-9938-d41958f0b54c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 384,
            "speed": 100
        },
        {
            "id": "02e107a0-1845-493c-a87c-a32411141cd4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 256,
            "speed": 100
        },
        {
            "id": "2682165b-8c33-4975-9206-0f5323ef78b8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 256,
            "speed": 100
        },
        {
            "id": "08a4aed6-6fa2-40f8-8283-c26bfb6be0c3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 320,
            "speed": 100
        },
        {
            "id": "7d9b8761-7857-4a56-befc-05cbe1619aa8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 320,
            "speed": 100
        },
        {
            "id": "8329fe76-8081-46ce-a234-d4c9252f657e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 352,
            "speed": 100
        },
        {
            "id": "804bea2e-00c5-4eb6-9e07-1901227ca86d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 352,
            "speed": 100
        },
        {
            "id": "a2ddcb8d-f366-4fe1-94b7-9b28dd614c5b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 192,
            "speed": 100
        },
        {
            "id": "765e7f36-ceef-4eb9-9291-65ba79a52a48",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 192,
            "speed": 100
        },
        {
            "id": "c25f0a1f-b214-4408-ad5a-4789c997bff4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 192,
            "speed": 100
        },
        {
            "id": "178fc7b2-69d5-458e-b561-5d712916f79e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 192,
            "speed": 100
        },
        {
            "id": "1fe0fe2a-c1f1-44c5-acc2-7868bd267e0d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 96,
            "speed": 100
        },
        {
            "id": "f57051cb-b6d7-47a0-ae1b-ec640cff5854",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 96,
            "speed": 100
        },
        {
            "id": "4f8cf53b-72c5-4ceb-8099-d2ee9198a095",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 160,
            "speed": 100
        },
        {
            "id": "c1e9d003-c51d-4403-a8e1-c56b499c8d53",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 160,
            "speed": 100
        },
        {
            "id": "3ea81742-7395-494b-bb55-ddc7d535f756",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 192,
            "speed": 100
        },
        {
            "id": "5033e571-077f-43f3-bd76-ae8cf58aa4d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 192,
            "speed": 100
        },
        {
            "id": "9e41b30b-3031-4b5f-ba5d-9c2e4b138226",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 160,
            "speed": 100
        },
        {
            "id": "534ab2ca-12ab-4553-bf80-6e2f2c7d1bde",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 160,
            "speed": 100
        },
        {
            "id": "da1b13a9-b0eb-4ac0-8942-7487371dfcca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896,
            "y": 224,
            "speed": 100
        },
        {
            "id": "68765d37-5872-45ae-8d81-a8f27ca9e1c7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 224,
            "speed": 100
        },
        {
            "id": "d1125e5a-e9b6-425d-b14d-05b85e06340d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 224,
            "speed": 100
        },
        {
            "id": "38b94b28-9ba0-4ebb-b660-2587c133dfd8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 192,
            "speed": 100
        },
        {
            "id": "a73735c9-d980-4b7e-a9c3-2ae43b366b17",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 192,
            "speed": 100
        },
        {
            "id": "3d7dc9d9-2685-496d-a4b2-a6a75bb1df50",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 192,
            "speed": 100
        },
        {
            "id": "3b73c4d7-12b4-463a-a14b-e904860c3162",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 64,
            "speed": 100
        },
        {
            "id": "877d5543-a2fa-4260-8703-5136f725a529",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 64,
            "speed": 100
        },
        {
            "id": "984b01d0-af00-47ba-bcdb-fe736d0658e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 96,
            "speed": 100
        },
        {
            "id": "cb87fe26-94ce-42a4-a210-3ba06b6ebb77",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 96,
            "speed": 100
        },
        {
            "id": "633129a4-6dc4-45b5-8e08-25ae828ffcc8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 32,
            "speed": 100
        },
        {
            "id": "4ef54acd-61db-429c-9068-c3ad075cd052",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 32,
            "speed": 100
        },
        {
            "id": "623748a4-3105-407f-9884-e3932a157c00",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 32,
            "speed": 100
        },
        {
            "id": "21cdeacf-e5df-4a37-85ee-25329b3119dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 96,
            "speed": 100
        },
        {
            "id": "7c32e218-b473-4552-a90d-95939c97c236",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 96,
            "speed": 100
        },
        {
            "id": "eef6fe71-7c9e-4aff-a030-9587c2cf1606",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 32,
            "speed": 100
        },
        {
            "id": "0abc6603-fba2-4bee-b24e-cd81fe1dd42b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 32,
            "speed": 100
        },
        {
            "id": "eb07c33b-a340-4f37-ab77-eb7a6f61ab5d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 352,
            "speed": 100
        },
        {
            "id": "b4dc5cc4-85b3-4b99-8f3c-58cb8dc4fe2a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 352,
            "speed": 100
        },
        {
            "id": "a7ded7f7-e5d4-4ef0-9f35-2f4a937c6797",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 192,
            "speed": 100
        },
        {
            "id": "85604493-6d1d-4c23-a34a-8b79eca06ae4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 192,
            "speed": 100
        },
        {
            "id": "4db05cd1-2695-48ee-b9ee-66975b3ffec2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 256,
            "speed": 100
        },
        {
            "id": "04a369cd-b849-475b-962c-776520e226e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 256,
            "speed": 100
        },
        {
            "id": "f1949df7-167c-4bf4-ae7a-fce7d5f07449",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 480,
            "speed": 100
        },
        {
            "id": "679a1d67-0d96-4b4b-9fe5-c8d8ab413ecc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 480,
            "speed": 100
        },
        {
            "id": "af9b9ea1-22d3-4871-8d50-decf0a246b9f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 480,
            "speed": 100
        },
        {
            "id": "df994b22-c610-412a-b0f8-348085da3a4c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 480,
            "speed": 100
        },
        {
            "id": "58014957-4c12-4325-89d5-46397671ebaf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 416,
            "speed": 100
        },
        {
            "id": "45031ec0-36ae-4cc9-83c7-519de51cc57c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 416,
            "speed": 100
        },
        {
            "id": "213eada4-3c08-4123-84a3-3883edfaa2d9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 416,
            "speed": 100
        },
        {
            "id": "fdd746c8-a86f-4144-bbb5-fc680135589c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 480,
            "speed": 100
        },
        {
            "id": "a45f5da9-de15-4413-8b73-4b006f44b41f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 480,
            "speed": 100
        },
        {
            "id": "5c8418ab-afbe-4643-a8af-b3aafaecc2fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 544,
            "speed": 100
        },
        {
            "id": "19d0bba0-1128-4b78-a2db-34bfe0e462e5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 544,
            "speed": 100
        },
        {
            "id": "c543dbc2-c4d1-4bea-a4c3-4af690c2dbf1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 544,
            "speed": 100
        },
        {
            "id": "0e5dfebd-6c3b-4161-88ab-addc922d5cee",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 608,
            "speed": 100
        },
        {
            "id": "7ed67e6a-e573-478c-8386-6bb1b81ec696",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 704,
            "speed": 100
        },
        {
            "id": "a9011e4e-6aee-409d-a39c-43a01c8ca801",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 704,
            "speed": 100
        },
        {
            "id": "68748269-ac48-447d-84ca-87039a41e0ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 608,
            "speed": 100
        },
        {
            "id": "db452631-b655-425a-8e9b-e679c15c4335",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 608,
            "speed": 100
        },
        {
            "id": "8b227fe6-ed98-490d-9b02-6edd14f5b252",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 608,
            "speed": 100
        },
        {
            "id": "c7ea7d27-8508-4171-84b8-917cabf3a7c8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 608,
            "speed": 100
        },
        {
            "id": "d4eca8ae-90b1-4bc2-b340-004fb48f7f4b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 672,
            "speed": 100
        },
        {
            "id": "bd17bedb-8f27-4e87-b0e1-87572b9f535b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 672,
            "speed": 100
        },
        {
            "id": "d562183a-6d30-4d30-a51b-f32d199d70b9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 672,
            "speed": 100
        },
        {
            "id": "8f92466b-6682-417e-b4fa-e0f41d669699",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 576,
            "speed": 100
        },
        {
            "id": "1db71bff-f95a-4a3c-9047-760b22b7ac4f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 576,
            "speed": 100
        },
        {
            "id": "a4a671ae-b64c-401a-8345-344200735f1d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 544,
            "speed": 100
        },
        {
            "id": "f2296ed6-a1bb-4ad9-a364-f6670ed41efb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 544,
            "speed": 100
        },
        {
            "id": "3dcfcd3d-96fc-4c8d-918f-08352e30ef0d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 512,
            "speed": 100
        },
        {
            "id": "1654b9ff-d738-409f-8b5a-b1b0d710bef3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 512,
            "speed": 100
        },
        {
            "id": "681704e2-104a-409b-b8c7-d273fe7760a4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 480,
            "speed": 100
        },
        {
            "id": "80b1da97-1e12-47dd-95a6-ebd10eab18ee",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 480,
            "speed": 100
        },
        {
            "id": "8d1c806d-e4ba-43d9-9ed2-3df146212fee",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 448,
            "speed": 100
        },
        {
            "id": "dda23dfb-4c86-43d1-8bf8-92c5c666b05c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 448,
            "speed": 100
        },
        {
            "id": "27ddee39-5733-45dd-a591-a2740c3bb286",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 416,
            "speed": 100
        },
        {
            "id": "76b6877a-e038-46b0-873b-21227dcfe949",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 384,
            "speed": 100
        },
        {
            "id": "7f27f064-cc6c-425c-84d8-e01c5ba20181",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 384,
            "speed": 100
        },
        {
            "id": "1d33e740-abcd-40f6-a04f-a1b4448d6e80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 352,
            "speed": 100
        },
        {
            "id": "f64753d4-55cb-4dc3-a065-64c4e3eae0d4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 352,
            "speed": 100
        },
        {
            "id": "cc48e35a-4688-4f9c-ab5c-673a7629f4ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 320,
            "speed": 100
        },
        {
            "id": "10d20263-3da3-4e79-95e9-03a411cbf39f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 320,
            "speed": 100
        },
        {
            "id": "10857df7-deb4-438c-a0b2-fc29c9ba760d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 256,
            "speed": 100
        },
        {
            "id": "22150515-6341-4d44-91e8-e2ee4f4e94bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 256,
            "speed": 100
        },
        {
            "id": "1f46de53-f443-4e15-8da3-2513bfa54716",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 224,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}